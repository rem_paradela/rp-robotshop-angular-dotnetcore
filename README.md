# Linc Technology-Partner Recruitment Project #

This project is made using Asp.Net Core, Angular 5 and Bootstrap 3, 
and is based on the Visual Studio template for Single Page Applications with
Angular.

To test this app go to https://rprobotstoreweb.azurewebsites.net/

In the main page you'll see a list of 4 robots, and there's one button in the page bottom 
to load more robots.

When you hover the mouse on one robot the delete button will appear.
On the left panel there's a button to create a new robot.


