import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ManagerService } from '../manager.service';
import { LoggerService } from '../logger.service';
import { Subscriber } from 'rxjs/Subscriber';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  private _managerService: ManagerService;
  private _logger: LoggerService;

  constructor(managerService: ManagerService, logger: LoggerService) {
    this._managerService = managerService;
    this._logger = logger;
  }

  ngOnInit() {
  }

  public create(form: NgForm) {
    this._managerService.createRobot(form.value["name"], form.value["desc"], form.value["price"], '/images/new_robot.jpg')
      .subscribe(result => {
        form.reset();
        alert("Robot created with success!");
      }, error => this._logger.error(error));
  }

}
