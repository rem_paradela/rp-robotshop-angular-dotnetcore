import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { ManagerComponent } from './manager/manager.component';
import { ManagerService } from './manager.service';
import { LoggerService } from './logger.service';
import { CreateComponent } from './create/create.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    ManagerComponent,
    CreateComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: ManagerComponent , pathMatch: 'full' },
      { path: 'create', component: CreateComponent },
    ])
  ],
  providers: [ManagerService, LoggerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
