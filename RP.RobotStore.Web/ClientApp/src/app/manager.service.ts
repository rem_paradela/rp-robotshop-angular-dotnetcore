import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ManagerService {
  public readonly ORDER_BY_NAME = "name";
  public readonly ORDER_BY_PRICE = "price";

  private baseUrl: string;
  private http: HttpClient;

  constructor(http: HttpClient, @Inject("BASE_URL") baseUrl: string) {
    this.http = http;
    this.baseUrl = baseUrl;
  }

  getRobots(offset: number, limit: number, orderBy: string = this.ORDER_BY_NAME, asc: boolean = true) {
    var qs = `?offset=${offset}&limit=${limit}&order=${orderBy}&asc=${asc}`;
    return this.http.get<Robot[]>(this.baseUrl + 'api/robots/' + qs);
  }

  deleteRobot(id: string) {
    var qs = `?id=${id}`;
    return this.http.delete(this.baseUrl + 'api/robots/' + qs);
  }

  createRobot(name: string, desc: string, price: number, image: string) {
    var robot = {
      name: name,
      description: desc,
      price: price,
      photo: image
    };

    return this.http.post(this.baseUrl + 'api/robots/', robot);
  }

}

export interface Robot {
  id: string,
  name: string,
  description: string,
  price: number,
  photo: string
}
