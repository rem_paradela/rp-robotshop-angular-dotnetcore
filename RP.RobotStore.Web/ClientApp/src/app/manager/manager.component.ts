import { Component, OnInit, Injectable } from '@angular/core';
import { ManagerService, Robot } from '../manager.service';
import { LoggerService } from '../logger.service';
import { forEach } from '@angular/router/src/utils/collection';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {
  private readonly pageSize = 4;
  private _sanitizer: DomSanitizer;
  private _managerService: ManagerService;
  private _loggerService: LoggerService;
  public robots: Robot[][];
  private loaded: number;

  constructor(managerService: ManagerService, loggerService: LoggerService, sanitizer: DomSanitizer) {
    this._managerService = managerService;
    this._loggerService = loggerService;
    this.robots = new Array<Robot[]>();
    this._sanitizer = sanitizer;
    this.loaded = 0;
  }

  ngOnInit() {
    this._managerService.getRobots(0, this.pageSize).subscribe(result => {
      this.addRobots(result);
      this.loaded = result.length;
    }, error => this._loggerService.error(error));
  }

  public loadMore() {
    this._managerService.getRobots(this.loaded, this.pageSize).subscribe(result => {
      this.addRobots(result);
      this.loaded += result.length;
    }, error => this._loggerService.error(error));
  }

  public delete(id: string) {
    this._managerService.deleteRobot(id).subscribe(result => {
      var list = this.getRendered(id);
      this.robots = new Array<Robot[]>();
      this.addRobots(list);
      this.loaded -= 1;
    }, error => this._loggerService.error(error));
  }

  public getBackground(image: string) {
    this._sanitizer.bypassSecurityTrustStyle(`url(${image})`)
  }

  private getRendered(idToSkip: string): Robot[] {
    if (this.robots != null) {
      var list = new Array<Robot>();
      for (let chunck of this.robots) {
        for (let robot of chunck) {
          if (robot.id != idToSkip) {
            list.push(robot);
          }
        }
      }

      return list;
    }
    else {
      return new Array<Robot>();
    }
  }

  private addRobots(list: Robot[]) {

    if (this.robots.length == 0) {
      this.robots.push(new Array());
    }

    for (let robot of list) {
      var current = this.robots[this.robots.length - 1];
      if (current.length == 4) {
        this.robots.push(new Array());
        current = this.robots[this.robots.length - 1];
      }

      current.push(robot);
    }

  }
}
