﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RP.RobotStore.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RP.RobotStore.Web.Controllers
{
    public class BaseApiController : Controller
    {
        public IMapper Mapper { get; set; }
        public StoreDbContext DbContext { get; set; }

        public BaseApiController(IMapper mapper, StoreDbContext context)
        {
            Mapper = mapper;
            DbContext = context;
        }
    }
}
