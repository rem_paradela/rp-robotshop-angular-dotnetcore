using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RP.RobotStore.Web.Models;
using RP.RobotStore.Web.Models.Dto;

namespace RP.RobotStore.Web.Controllers
{
    [Route("api/[controller]")]
    public class RobotsController : BaseApiController
    {
        public RobotsController(IMapper mapper, StoreDbContext context)
            : base(mapper, context)
        { }

        [HttpGet("")]
        public IEnumerable<RobotDto> ListRobots(int offset = 0, int limit = 10)
        {
            return DbContext.Robots.Skip(offset).Take(limit).Select(x => Mapper.Map<RobotDto>(x));

        }

        [HttpDelete("")]
        public void Delete(Guid id)
        {
            var robot = DbContext.Robots.FirstOrDefault(x => x.Id == id);
            if (robot != null)
            {
                DbContext.Robots.Remove(robot);
                DbContext.SaveChanges();
            }
        }

        [HttpPost("")]
        public void Create([FromBody]RobotDto dto)
        {
            var newRobot = Mapper.Map<Robot>(dto);
            newRobot.Id = Guid.NewGuid();
            DbContext.Robots.Add(newRobot);
            DbContext.SaveChanges();
        }
    }
}
