﻿using AutoMapper;
using RP.RobotStore.Web.Models.Dto;

namespace RP.RobotStore.Web.Models.Mappers
{
    public class RobotProfile : Profile
    {
        public RobotProfile()
        {
            CreateMap<Robot, RobotDto>();
            CreateMap<RobotDto, Robot>();
        }
    }
}
