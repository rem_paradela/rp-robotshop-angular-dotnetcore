﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RP.RobotStore.Web.Models
{
    public class Robot
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string Photo { get; set; }
    }
}
