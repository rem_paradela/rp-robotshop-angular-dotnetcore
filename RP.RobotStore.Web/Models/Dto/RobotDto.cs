﻿using Newtonsoft.Json;
using System;

namespace RP.RobotStore.Web.Models.Dto
{
    public class RobotDto
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("price")]
        public decimal Price { get; set; }
        [JsonProperty("photo")]
        public string Photo { get; set; }
    }
}
