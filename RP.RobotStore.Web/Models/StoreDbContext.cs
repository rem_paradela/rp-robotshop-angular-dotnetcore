﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RP.RobotStore.Web.Models
{
    public class StoreDbContext : DbContext
    {
        public DbSet<Robot> Robots { get; set; }

        public StoreDbContext(DbContextOptions<StoreDbContext> options)
        : base(options)
        { }

        public void Init()
        {
            Robots.AddRange(
              new Robot
              {
                  Id = Guid.NewGuid(),
                  Name = "Robot 1",
                  Description = "Small robot with wheels",
                  Price = 10.50m,
                  Photo = "/images/robot1.jpg"
              },
                new Robot
                {
                    Id = Guid.NewGuid(),
                    Name = "Robot 2",
                    Description = "Bit robot with arms and legs",
                    Price = 40.00m,
                    Photo = "/images/robot2.jpg"
                },
                new Robot
                {
                    Id = Guid.NewGuid(),
                    Name = "Robot 3",
                    Description = "Small robot with wheels, arms and small screen",
                    Price = 19.99m,
                    Photo = "/images/robot3.jpg"
                },
                new Robot
                {
                    Id = Guid.NewGuid(),
                    Name = "Robot 4",
                    Description = "Big robot with wheels",
                    Price = 20.50m,
                    Photo = "/images/robot4.jpg"
                },
                new Robot
                {
                    Id = Guid.NewGuid(),
                    Name = "Robot 2",
                    Description = "Bit robot with arms and legs",
                    Price = 40.00m,
                    Photo = "/images/robot2.jpg"
                },
                new Robot
                {
                    Id = Guid.NewGuid(),
                    Name = "Robot 3",
                    Description = "Small robot with wheels, arms and small screen",
                    Price = 19.99m,
                    Photo = "/images/robot3.jpg"
                },
                new Robot
                {
                    Id = Guid.NewGuid(),
                    Name = "Robot 4",
                    Description = "Big robot with wheels",
                    Price = 20.50m,
                    Photo = "/images/robot4.jpg"
                }
            );

            SaveChanges();
        }
    }
}
